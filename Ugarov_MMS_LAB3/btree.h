#pragma once


int Dt[12] = { 27, 17, 15 ,23, 6,91,18,12,9,4,52,89 };
int u;



class btree
{
protected:
	int Dn; int B; btree *Ln; btree *Rn;
public:
	btree() { B = 1; }
	int Empty() { return (B == 1); }
	void Add(int D);
	void Pri(int k);
	void Destroy();
	int MLevel(int &ml, int u);
	void addbalans(int N);
	int CPLevel(int &CPL, int L, int u);
	int MyMethod(int &CPL, int L, int u);
	int MyMethod2(int &CPL, int L, int u);
private:
	bool chsimple(int x);
	bool chdual(int x);
};

//�������� � ������ ������ ��������� �������
void btree::Add(int D)
{
	if (B)
	{
		B = 0;
		Dn = D;
		Ln = new(btree);
		Rn = new(btree);
	}
	else {
		if (D < Dn)
			Ln->Add(D);
		else
			Rn->Add(D);
	}
}

//������ ��������� ������ 
void btree::Pri(int k)
{
	if (!B)
	{
		Ln->Pri(k + 4);
		for (int i = 0; i <= k; ++i)
			cout << " ";
		cout << Dn << " " << endl;
		Rn->Pri(k + 4);
	}
}

//�������� ��������� ������ 
void btree::Destroy()
{
	if (!B)
	{
		Ln->Destroy(); delete(Ln);
		Rn->Destroy(); delete(Rn);
	}
	B = 1;
}

//������������ ������� ������
int btree::MLevel(int &mL, int u)
{
	if (!B)
	{
		if (u > mL)
			mL = u;
		Ln->MLevel(mL, u + 1);
		Rn->MLevel(mL, u + 1);
	}
	return mL;
}

//����� ��� ������� ������� ������ ����� ��������� �� �������� ������ L
int btree::CPLevel(int &CPL, int L, int u)
{
	if (!B)
	{
		if (u == L&&Dn % 2 == 0)
			++CPL;
		Ln->CPLevel(CPL, L, u + 1);
		Rn->CPLevel(CPL, L, u + 1);
	}
	return CPL;
}

//���������� ����������������� ������ 
void btree::addbalans(int N)
{
	if (N != 0)
	{
		B = 0;
		Dn = Dt[u];
		++u;
		Ln = new(btree);
		Rn = new(btree);
		int NL = N / 2;
		int NR = N - NL - 1;
		Ln->addbalans(NL);
		Rn->addbalans(NR);
	}
}


//����� ��� ������� ������� ������� ����� ��������� �� �������� ������ L
int btree::MyMethod(int &CPL, int L, int u)
{
	if (!B)
	{
		if (u == L && chsimple(Dn))
			++CPL;
		Ln->MyMethod(CPL, L, u + 1);
		Rn->MyMethod(CPL, L, u + 1);
	}
	return CPL;
}

//����� ��� ������� ������� ���������� ����� ��������� �� �������� ������ L
int btree::MyMethod2(int &CPL, int L, int u)
{
	if (!B)
	{
		if (u == L && chdual(Dn))
			++CPL;
		Ln->MyMethod2(CPL, L, u + 1);
		Rn->MyMethod2(CPL, L, u + 1);
	}
	return CPL;
}

//�������� �� ��������
bool btree::chsimple(int x)
{
	for (int i = 2; i <= sqrt(x); ++i)
		if (x%i == 0)
			return false;
	return true;
}

//�������� �� ���������� �����
bool btree::chdual(int x)
{
	x = abs(x);
	if (x > 9 && x < 100)
		return true;
	return false;
}